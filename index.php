<?php include "config.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Primary Meta Tags -->
	<title><?= $title ?></title>
	<meta name="title" content="<?= $title ?>">
	<meta name="description" content="<?= $description ?>">

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:url" content="<?= $url ?>">
	<meta property="og:title" content="<?= $title ?>">
	<meta property="og:description" content="<?= $description ?>">
	<meta property="og:image" content="<?= $image ?>">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:url" content="<?= $url ?>">
	<meta property="twitter:title" content="<?= $title ?>">
	<meta property="twitter:description" content="<?= $description ?>">
	<meta property="twitter:image" content="<?= $image ?>">

	<link rel="apple-touch-icon" sizes="57x57" href="assets/icon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/icon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/icon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/icon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/icon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/icon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/icon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/icon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="assets/icon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="assets/icon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/icon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/icon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/icon/favicon-16x16.png">
	<link rel="manifest" href="assets/icon/manifest.json">
	<meta name="msapplication-TileColor" content="#ECF3FF">
	<meta name="msapplication-TileImage" content="assets/icon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ECF3FF">

	<link rel="stylesheet" href="assets/vendors/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendors/toastr/toastr.min.css">
	<style>
		.logo img {
			width: 240px;
			max-width: 100%;
		}

		textarea.textarea-custom {
			height: calc(1.5em + 0.75rem + 2px);
			cursor: copy;
			resize: none;
		}

		header,
		footer {
			background: #ECF3FF;
		}

		header {
			padding: 15px;
		}

		footer {
			position: fixed;
			bottom: 0;
			width: 100%;
			padding: 10px;
		}
	</style>
	<script>
		function copyToClipboard(el) {
			var copyText = $(el);
			copyText.select();
			document.execCommand("copy");
			toastr.success("Success Copied !");
		}
	</script>
</head>

<body>
	<header>
		<div class="text-center">
			<a href="<?= $url ?>" class="logo"><img class="logo" src="assets/images/logo.png" alt="<?= $title ?>"></a>
		</div>
	</header>

	<div class="container-fluid my-3">
		<div class="mb-2">
			<div class="font-weight-bold">Laravel 10</div>
			<textarea title="Copy" readonly class="form-control textarea-custom" onclick="copyToClipboard(this)">composer create-project laravel/laravel:^10.0 laravel10</textarea>
		</div>
		<div class="mb-2">
			<div class="font-weight-bold">Debugbar</div>
			<textarea title="Copy" readonly class="form-control textarea-custom" onclick="copyToClipboard(this)">composer require barryvdh/laravel-debugbar --dev</textarea>
			<textarea title="Copy" readonly class="form-control textarea-custom" onclick="copyToClipboard(this)">php artisan vendor:publish --provider="Barryvdh\Debugbar\ServiceProvider"</textarea>
		</div>
		<div class="mb-2">
			<div class="font-weight-bold">Laravel UI</div>
			<textarea title="Copy" readonly class="form-control textarea-custom" onclick="copyToClipboard(this)">composer require laravel/ui</textarea>
		</div>

	</div>
	<footer>
		<div class="text-center">&copy; 2022. By <a href="javascript:;">gungcahyadipp</a></div>
	</footer>

	<script src="assets/vendors/jquery/jquery.min.js"></script>
	<script src="assets/vendors/toastr/toastr.min.js"></script>
</body>

</html>